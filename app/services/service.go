package services

import (
	"app/common"
	"app/model"
	"os"

	"github.com/joho/godotenv"
	"gopkg.in/mgo.v2/bson"
)

func init() {
	if len(os.Getenv("MONGO_USER")) == 0 {
		err := godotenv.Load()
		common.CheckOnError(err)
	}
	if len(os.Getenv("MONGO_PSWD")) == 0 {
		err := godotenv.Load()
		common.CheckOnError(err)
	}
	if len(os.Getenv("MONGO_HOST")) == 0 {
		err := godotenv.Load()
		common.CheckOnError(err)
	}
	common.ConnectDB()
}

// CreateBike function adds new Bike objects into the database
func CreateBike(bike model.Bike) (string, error) {
	var err error
	s := common.Session.Clone()
	defer s.Close()
	bike.ID = bson.NewObjectId()
	bike.Available = "true"
	err = common.BikesCollection.Insert(&bike)
	return bike.ID.Hex(), err
}

// DeleteBike function removes an existing objects from the database
func DeleteBike(id string) error {
	_, err := GetBike(id)
	if err == nil {
		oID := bson.ObjectIdHex(id)
		err = common.BikesCollection.Remove(bson.M{"_id": oID})
	}
	return err
}

// UpdateBike function sets a bike's availability to true or false
func UpdateBike(id string) (string, error) {
	bike, err := GetBike(id)
	if bike.Available == "true" {
		bike.Available = "false"
	} else {
		bike.Available = "true"
	}
	oID := bson.ObjectIdHex(id)
	if err == nil {
		err = common.BikesCollection.Update(bson.M{"_id": oID}, bike)
	}
	return bike.Available, err
}

// GetBike function retrieves Bike objects from the database
func GetBike(id string) (model.Bike, error) {
	bike := model.Bike{}
	oID := bson.ObjectIdHex(id)
	err := common.BikesCollection.FindId(oID).One(&bike)
	return bike, err
}

// ListBike function retrieves all Bike objects from the database
func ListBike() ([]model.Bike, error) {
	bikes := []model.Bike{}
	err := common.BikesCollection.Find(nil).Sort("-updated_on").All(&bikes)
	return bikes, err
}
