package main

import (
	"app/handlers"

	"github.com/gin-gonic/gin"
)

func main() {
	router := gin.Default()

	router.Use(gin.Logger())
	router.Use(gin.Recovery())

	router.POST("/bikes/create", handlers.Create)
	router.DELETE("/bikes/delete/:id", handlers.Delete)
	router.PUT("/bikes/update/:id", handlers.Update)
	router.GET("/bikes/get/:id", handlers.Get)
	router.GET("/bikes/get", handlers.List)

	router.Run("0.0.0.0:3001")
}
