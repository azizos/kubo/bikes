package common

import (
	"crypto/tls"
	"net"
	"os"

	"github.com/op/go-logging"
	"gopkg.in/mgo.v2"
)

// BikesCollection is a collection for the presisted Bikes
var BikesCollection *mgo.Collection

// Session is used to interact with the bikes databases
var Session *mgo.Session

var log, _ = GetLogModule()

// GetLogModule is used to create and return a formatted logging module to be used globally in app
func GetLogModule() (*logging.Logger, error) {
	var format = logging.MustStringFormatter(
		`%{color}%{time:15:04:05.000} %{shortfunc} ▶ %{level:.6s} %{id:03x}%{color:reset} %{message}`,
	)
	logging.SetFormatter(format)
	return logging.GetLogger("app")
}

// CheckOnError checks on any fatal errors and stops the application
func CheckOnError(e error) {
	var log, _ = GetLogModule()
	if e != nil {
		log.Fatal(e)
	}
}

// ConnectDB makes connection with the database
func ConnectDB() {
	log.Info("Connecting to the database..")
	mongoUser := os.Getenv("MONGO_USER")
	mongoPswd := os.Getenv("MONGO_PSWD")
	mongoHost := os.Getenv("MONGO_HOST")

	mongoURI := "mongodb://" + mongoUser + ":" + mongoPswd + "@" + mongoHost + "/bikes?authSource=admin"

	dialInfo, err := mgo.ParseURL(mongoURI)
	CheckOnError(err)
	tlsConfig := &tls.Config{}
	dialInfo.DialServer = func(addr *mgo.ServerAddr) (net.Conn, error) {
		conn, err := tls.Dial("tcp", addr.String(), tlsConfig)
		return conn, err
	}
	Session, _ = mgo.DialWithInfo(dialInfo)
	CheckOnError(err)
	log.Info("Connected to the database")
	Session.SetMode(mgo.Monotonic, true)
	BikesCollection = Session.DB("kubo").C("bikes")
}
