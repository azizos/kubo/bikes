package handlers

import (
	"app/common"
	"app/model"
	"app/services"
	"net/http"

	"github.com/gin-gonic/gin"
)

var log, _ = common.GetLogModule()

// Create function handles the HTTP requests to create a new Bike in the database
func Create(context *gin.Context) {
	var bike model.Bike
	if err := context.ShouldBindJSON(&bike); err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	foundID, err := services.CreateBike(bike)

	if err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"message": "no bike could be created in the database"})
	} else {
		log.Info("a new bike has been addedd:" + foundID)

		context.JSON(http.StatusOK, gin.H{"id": foundID})
	}
}

// Delete function handles the HTTP requests to delete an existing Bike in the database
func Delete(context *gin.Context) {
	err := services.DeleteBike(context.Param("id"))
	if err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"message": "bike could not be removed"})
	} else {
		context.JSON(http.StatusOK, gin.H{"message": "bike has been removed"})
	}
}

// Update function updates a bike's availability to true or false
func Update(context *gin.Context) {
	bikeAvailability, err := services.UpdateBike(context.Param("id"))
	if err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"message": "bike's availability could not change"})
	} else {
		context.JSON(http.StatusOK, gin.H{"message": "bike's availability has changed to " + bikeAvailability})
	}
}

// Get function handles the HTTP requests to retrieve Bike objects from the database
func Get(context *gin.Context) {
	foundBike, err := services.GetBike(context.Param("id"))
	if err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"message": "no bike could be found in the database"})
	} else {
		context.JSON(http.StatusOK, foundBike)
	}
}

// List function handles the HTTP requests to retrieve all Bike objects from the database
func List(context *gin.Context) {
	bikes, err := services.ListBike()
	if err != nil {
		context.JSON(http.StatusOK, gin.H{"message": "something went wrong, please try again.."})
	} else {
		if len(bikes) <= 0 {
			context.JSON(http.StatusOK, gin.H{"message": "oops, no bikes found :("})
		}
		context.JSON(http.StatusOK, bikes)
	}
}
