package model

import (
	"gopkg.in/mgo.v2/bson"
)

// Bike :The number of properties needed to setup a new MVP project
type Bike struct {
	ID        bson.ObjectId `bson:"_id,omitempty"`
	Brand     string        `json:"brand,omitempty" binding:"required" bson:",omitempty"`
	Series    string        `json:"series,omitempty" binding:"required" bson:",omitempty"`
	Frame     string        `json:"frame,omitempty" binding:"required" bson:",omitempty"`
	Rate      string        `json:"rate,omitempty" binding:"required" bson:",omitempty"`
	Available string        `json:"available,omitempty" bson:",omitempty"`
}

// [
// {
// 	"brand": "Gazelle",
// 	"series": "Orange",
// 	"frame": "Male bike",
// 	"rate":	"10E",
// 	"status": "false"
// },
// 	{
// 		"brand": "Cube",
// 		"series": "Cross country",
// 		"frame": "Male bike",
// 		"rate":	"22E",
// 		"status": "false"
// 	},
// 	{
// 		"brand": "Gazelle",
// 		"series": "Cityzen",
// 		"frame": "Male bike",
// 		"rate":	"33E",
// 		"status": "false"
// 	},
// ]
