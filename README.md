# Bikes service

This project is used as a part of the entire Kubo application.
Bikes service is responsible to persist and manage all the bike entities.

# Use

Please find the Postman collection in the [infra](https://gitlab.com/azizoo/kubo/infra) repository.
